import mongoose from 'mongoose';

mongoose.connect('mongodb://mongo/challengemongodb')
    .then(db => console.log('DB is connected to', db.connection.host))
    .catch(err => console.error(err))