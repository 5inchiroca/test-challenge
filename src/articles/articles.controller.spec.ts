import { Test, TestingModule } from '@nestjs/testing';
import { Paginate } from 'src/utils';
import { ArticlesController } from './articles.controller';
import { ArticlesService, PaginateArticle } from './articles.service';
import { Article } from './schemas/article.schema';

describe('ArticlesController', () => {
  let controller: ArticlesController;

  const fakeArticleId = '617000000619504b4b4663b5';

  const fakeArticle = {
    _id: fakeArticleId,
    title: 'Lorem ipsum'
  };

  const mockArticlesService = {
      create: jest.fn(dto => ({
        _id: Date.now() + '',
        ...dto
      })),
      findAll: jest.fn(pagParams => ({
        ...pagParams,
        count: Math.floor(Math.random() * (100)),
        data: [fakeArticle]
      })),
      findOne: jest.fn(id => fakeArticle),
      update: jest.fn((id, dto) => ({
        ...fakeArticle,
        ...dto
      })),
      remove: jest.fn(id => fakeArticle)
    },
    articleDto = {
      title: 'myTitleTest',
      author: 'myAuthorTest'
    },
    anotherArticleDto = {
      title: 'Another title',
      author: 'anotherAuthor'
    },
    paginateParams = {
      page: '1',
      limit: '5'
    };


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).overrideProvider(ArticlesService).useValue(mockArticlesService).compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create an article', async () => {
    expect(await controller.create(articleDto)).toEqual({
      _id: expect.any(String),
      ...articleDto
    });

    expect(mockArticlesService.create).toHaveBeenCalledWith(articleDto);
  });

  it('should get paginated articles', async () => {

    let articles: Article[],
      paginate: PaginateArticle;

    paginate = await controller.findAll(paginateParams);

    articles = paginate.data

    expect(paginate).toEqual({
      page: +paginateParams.page,
      limit: +paginateParams.limit,
      count: expect.any(Number),
      data: [fakeArticle]
    });

    expect(articles).toEqual([fakeArticle]);
  });

  it('should get an article', async () => {

    let article: Article;

    article = await controller.findOne(fakeArticleId);

    expect(article).toEqual(fakeArticle);
  });

  it('should update an article', async () => {

    expect(await controller.update(fakeArticleId, anotherArticleDto)).toEqual({...fakeArticle, ...anotherArticleDto});
  });

  it('should delete an article', async () => {

    expect(await controller.remove(fakeArticleId)).toEqual(fakeArticle);
  });
});
