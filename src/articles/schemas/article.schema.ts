import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {

    @Prop()
    title: string;

    @Prop()
    url: string;

    @Prop()
    author: string;

    @Prop({ default: 0 })
    points: number;

    @Prop()
    story_text: string;

    @Prop()
    comment_text: string;

    @Prop({ default: 0 })
    num_comments: number;

    @Prop()
    story_id: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    @Prop()
    parent_id: number;

    @Prop({ default: new Date() })
    created_at: Date;

    @Prop({ default: Math.floor(new Date().getTime() / 1000) })
    created_at_i: number;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);