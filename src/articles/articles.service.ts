import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Paginate, PaginationParams } from './../utils';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article, ArticleDocument } from './schemas/article.schema';

export class PaginateArticle extends Paginate {
  data: Article[];
}

@Injectable()
export class ArticlesService {

  private readonly logger = new Logger(ArticlesService.name);

  constructor(@InjectModel(Article.name) private articleModel: Model<ArticleDocument>, private httpService: HttpService){}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const createdArticle = new this.articleModel(createArticleDto);
    return createdArticle.save();
  }

  async findAll(paginationParams: PaginationParams): Promise<PaginateArticle> {

    const { page, limit } = paginationParams

    const count = await this.articleModel.count()

    const data = await this.articleModel.find().skip(limit * (page - 1)).limit(limit);

    return {
      page,
      limit,
      count,
      data
    }
  }

  async findOne(id: string): Promise<Article> {
    return this.articleModel.findById(id);
  }

  async update(id: string, updateArticleDto: UpdateArticleDto): Promise<Article> {
    return this.articleModel.findByIdAndUpdate(id, updateArticleDto, { new: true });
  }

  async remove(id: string): Promise<Article> {
    return this.articleModel.findByIdAndDelete(id)
  }

  @Cron('0 * * * *')
  insertDataApi() {

    this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').subscribe(res => {

      if( res.status == 200 ){

        const articles = res.data.hits;

        articles.map(art => {

          const createdArticle = new this.articleModel(art);
          createdArticle.save();
        })

        this.logger.debug('Articles stored!');

      } else {

        console.log('Something went wrong')
      }
    })
  }
}
