import { HttpModule, HttpService } from '@nestjs/axios';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { PaginationParams } from 'src/utils';
import { ArticlesService, PaginateArticle } from './articles.service';
import { Article } from './schemas/article.schema';

describe('ArticlesService', () => {
  let service: ArticlesService;

  const fakeArticleId = '617000000619504b4b4663b5';

  const articleDto = {
      title: 'myTitleTest',
      author: 'myAuthorTest'
    },
    fakeArticle = {
      _id: fakeArticleId,
      title: 'Lorem ipsum'
    };

  class mockArticleModel {
    constructor(private data) {}
    save = jest.fn().mockResolvedValue(this.data);
    count = jest.fn().mockResolvedValue(Math.floor(Math.random() * 100));
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArticlesService, {
        provide: getModelToken(Article.name),
        useValue: mockArticleModel
      }],
      imports: [HttpModule]
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create an article', async () => {

    const article = service.create(articleDto);

    expect(article).toEqual(Promise.resolve({
      _id: expect.any(String),
      ...articleDto
    }));
  });

  it('should get paginated articles', async () => {

    const paginateParams: PaginationParams = {
      page: 1,
      limit: 5
    };

    let articles: Article[],
      paginateArticle: PaginateArticle

      paginateArticle = await service.findAll(paginateParams);

    expect(paginateArticle).toEqual({
      page: paginateParams.page,
      limit: paginateParams.limit,
      count: expect.any(Number),
      data: [fakeArticle]
    });

    expect(articles).toEqual([fakeArticle]);
  });
});
