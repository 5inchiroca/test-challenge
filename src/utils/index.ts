export class PaginationParams {
    page: number;
    limit: number;
}

export class Paginate extends PaginationParams {
    count: number;
}